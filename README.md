# audio-recorder

A simple web component made with svelte to record and play your voice from your browser

## Setup
- download or clone
- run  ```yarn install ```  //or npm install
- grab built script  ```dist/AudioRecorder.mjs```  and put it in your HTML project

## Basic usage
```html
        <script type="module" src="./AudioRecorder.mjs"></script>
        //...
        <audio-recorder></audio-recorder>
```

## Multiple instances
```html
<script type="module" src="./AudioRecorder.mjs"></script>
//...
<audio-recorder id="myAudio"></audio-recorder>
<audio-recorder id="myAudio2"></audio-recorder>
```

## Get recorded audio
 The _change_ event allows to capture base64 audio
```html
    <script type="module" src="./AudioRecorder.mjs"></script>
    //...
    <audio-recorder id="myAudio"></audio-recorder>
    <script>
        const myAudio = document.querySelector("#myAudio");
        myAudio.addEventListener('change', (evt) => {
            console.log("Audio data", evt.detail);
            // Exemple : Send audio to server
        })
    </script>
```


